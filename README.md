# AzureLogging solution framework


##  description

We are requested to publish applications logs of various programs ( scripts, batch jobs, servers, ... ) to Azure eventhubs which is our gateway towards the ELK monitoring infrastructure.
In order to ease the process we are delivering a set of (small) apis to allow logging to eventhubs in json format, along with environmental context values, using the different languages natural logging apis.


For example on java we permit the use of log4j or log4j2 ( or  using a slf4j abstraction in front of them ).

On python  we permit the use of the logging logers standard package

On C#  we permit the use NLog famous logging framework


# Java 

See the readme in the java subfolder


# Python

See the readme in the python subfolder




# CSharp

See the readme in the CSharp subfolder
