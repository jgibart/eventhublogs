# AzureLogging solution framework for CSharp ( or dotnet )


##  description

We are requested to publish applications logs of various programs ( scripts, batch jobs, servers, ... ) to Azure eventhubs which is our gateway towards the ELK monitoring infrastructure.
In order to ease the process we are delivering a set of (small) apis to allow logging to eventhubs in json format, along with environmental context values, using the different languages natural logging apis.

 
On C#  we permit the use NLog famous logging framework

 
 
## dependencies
 
In order to operate properly the solution requires the following assemblies ( all available on nuget ):

```
    <PackageReference Include="Microsoft.Azure.EventHubs" Version="4.1.0" />
    <PackageReference Include="NLog.Config" Version="4.6.8" />
    <PackageReference Include="NLog.Extensions.AzureEventHub" Version="2.1.0" />
    <PackageReference Include="NLog.Extensions.Logging" Version="1.6.1" />
```


The use of the source file CsharpLogs.cs is not mandatory but this is the only way to be sure the logs are flushed upon process stop or end
There is a sample program in Program.cs showing the logger usage which has nothing special except the main code is protected by a try catch section :

```
        static void Main(string[] args)
        {

            //NLog recommends using a static variable for the logger object

            try
            {
                CsharpLogs.setupLogsAsync();
                run();
            } finally
            {
                CsharpLogs.shutdown();
            }
        }


```


All the intersting configuration stuff is in the NLog configuration file that must be deployed in the same folder as the program ( .exe) in the end

```
<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.nlog-project.org/schemas/NLog.xsd NLog.xsd"
      autoReload="true"
      throwExceptions="true"
      >
  <extensions>
    <add assembly="NLog.Extensions.AzureEventHub" />
  </extensions>
  <targets>
    <target xsi:type="AzureEventHub"
             name="eventhub"
              connectionString="Endpoint=sb://<target_eventhub_namespace>.servicebus.windows.net/;SharedAccessKeyName=xxxxx;SharedAccessKey=xxxxxxyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy=;EntityPath=<target_eventhub>;OperationTimeout=00:00:15;TransportType=AmqpWebSockets;">
     <contextProperty name="level" layout="${level}" />
      <contextProperty name="exception" layout="${exception:format=shorttype}" includeEmptyValue="false" />
      <layout type="JsonLayout" includeAllProperties="true">
        <attribute name="timestamp" layout="${date:universalTime=False:format=yyyy-MM-dd'T'HH\:mm\:ss.fffzz}" />
        <attribute name="log_message" layout="${message}" />
        <attribute name="criticality" layout="${level}" />
        <attribute name="exception" layout="${exception:format=tostring}" />
        <attribute name="branch" layout="TGS/TGITS" />
        <attribute name="application" layout="RTFLD" />
        <attribute name="environment" layout="hp" />
        <attribute name="source_component" layout="RTFLD-StreamIoT" />
        <attribute name="source_name" layout="RTFLD-StreamIoT" />
      </layout>
    </target>
    <!-- Using the colored console as my target-->
    <target xsi:type="ColoredConsole" name="colored_console">
    </target>
    </targets>
  <rules>
    <logger name="*" minlevel="Warn"
       writeTo="colored_console,eventhub" final="true"/>
    <logger name="csharplogging.*" minlevel="Debug"
       writeTo="colored_console,eventhub" />
  </rules>
</nlog>


```


## set your applicative log level 

In general the root logs are of no interest to you

in you code you have specialized loggers 
```
namespace <your namespace>
{
    class <yourclass>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
  ...
  }
}
```

This logger is the logger named <your namespace>.<yourclass>
More generally you will be interested of all loggers or your namespace.


You control the log level of your namespace in NLog.config with a rule like 
```
    <logger name="<your namespace>.*" minlevel="Info"
       writeTo="colored_console,eventhub" />
```

while you control the log level of all other namespaces 
```
    <logger name="*" minlevel="Warn"
       writeTo="colored_console,eventhub" final="true"/>

```

Levels can be Trace Debug Info Warn Error Fatal

## set your metadata

The json layout controls what is sent to eventhubs .

There are dynamic informations from the log itself ( date, severity, message ) as you see ${something}

And there are static fields taken from the configuration file :
 
 ```
         <attribute name="branch" layout="TGS/TGITS" />
        <attribute name="application" layout="<Your application>" />
        <attribute name="environment" layout="<the environment hp | prod | dev" />
        <attribute name="source_component" layout="<the program name>" />
        <attribute name="source_name" layout="<the machine name>" />
```

###  a word on proxying

If you need a http proxy to reach azure eventhubs, it must be done in your main program before you create your logger

```

namespace csharplogging
{
    class SampleProgram
    {
        static SampleProgram() {
           /*
            * 
            * if using a proxy, place the proxy instructions  before creating any logger
            * */
            #region Proxy
            ICredentials Credentials = null;
            //if dredentials are required uncomment below
            //Credentials = new NetworkCredential(proxyUser, proxyPassword);
            WebProxy proxy =    new WebProxy("<proxy_host>:<proxy_port>", false, null, Credentials);
            WebRequest.DefaultWebProxy = proxy;
            Console.WriteLine("proxy installed "+proxy);
            #endregion
          
         }
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static NLog.Logger rootlogger = NLog.LogManager.GetLogger("");

        static void Main(string[] args)
        {
        ...
        }
    }
}
```

Beware : 
proxying will work only if you have explicitely set in the connection string to use websockets amqp :
```
             connectionString="Endpoint=sb://<target_eventhub_namespace>.servicebus.windows.net/;SharedAccessKeyName=xxxxx;SharedAccessKey=xxxxxxyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy=;EntityPath=<target_eventhub>;OperationTimeout=00:00:15;TransportType=AmqpWebSockets;">

```