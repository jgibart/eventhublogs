﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using NLog;
using NLog.Targets;
using Microsoft.Azure.EventHubs;
using System.Net.Http;

namespace csharplogging
{
    //NLog.Extensions.AzureEventHub
    //NLog.Extensions.AzureEventHub
    class CsharpLogs
    {

        static public   void setupLogsAsync()
        {
            NLog.Targets.Target target = LogManager.Configuration.FindTargetByName("eventhub");
            if ( target == null)
            {
                Console.WriteLine("no target named eventhub in NLog.conf ");
                return;
            } else
            {

                Console.WriteLine("examining  eventhub target");
                EventHubTarget ehtarget = ((EventHubTarget)target);
                string connString = ehtarget.ConnectionString.ToString();
                Console.WriteLine("eventhub logging configured to " + connString );
                if (! connString.Contains("OperationTimeout="))
                {
                    Console.WriteLine("WARNING : in NLog.conf , eventhub target : no explicit OperationTimeout set in connection string. Please add ;OperationTimeout=00:00:15;");
                }
                if (!connString.Contains("TransportType=AmqpWebSockets"))
                {
                    Console.WriteLine("WARNING : in NLog.conf , eventhub target : no TransportType set in connection string. You should add ;TransportType=AmqpWebSockets; if you want to use a proxy");
                }

            }

        }
        static public void shutdown()
        {
            NLog.LogManager.Shutdown(); // Flush and close down internal threads and timers
        }
    }
}