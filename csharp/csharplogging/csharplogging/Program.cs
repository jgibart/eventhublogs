﻿using System;
using System.Net;
using Microsoft.Azure.EventHubs;

namespace csharplogging
{
    class SampleProgram
    {
        static SampleProgram() {
           /*
            * 
            * if using a proxy, place the proxy instructions  before creating any logger
            * */
            #region Proxy
             WebProxy proxy =    new WebProxy("127.0.0.1:8888", false, null);
            WebRequest.DefaultWebProxy = proxy;
            Console.WriteLine("proxy installed "+proxy);
            #endregion
          
         }
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static NLog.Logger rootlogger = NLog.LogManager.GetLogger("");

        static void Main(string[] args)
        {

            //NLog recommends using a static variable for the logger object

            try
            {
                CsharpLogs.setupLogsAsync();
                run();
            } finally
            {
                CsharpLogs.shutdown();
                //System.Threading.Thread.Sleep(30000);
            }
        }
        static void run() { 

                //NLog supports several logging levels, including INFO
            logger.Trace("Trace log");
            logger.Debug("Debug log");
            logger.Info("Info Info log");
            rootlogger.Info("Info from root logger");
            logger.Warn("Warn log");
            rootlogger.Warn("Warn from root logger");
            logger.Error("Error log");
            Exception ex = new Exception("this is just a test");
            //Exceptions are typically logged at the ERROR level
            logger.Error(ex, "a log with an exception");

            for ( int i  = 0; i < 10; i++)
            {
                logger.Info("Info message number "+i);

            }

        }
    }
}
