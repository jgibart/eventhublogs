# AzureLogging solution framework for Java ( or pyspark )


##  description

We are requested to publish applications logs of various programs ( scripts, batch jobs, servers, ... ) to Azure eventhubs which is our gateway towards the ELK monitoring infrastructure.
In order to ease the process we are delivering a set of (small) apis to allow logging to eventhubs in json format, along with environmental context values, using the different languages natural logging apis.


On java we permit the use of log4j or log4j2 .

On pyspark   we permit the use of the java log4j loggers

 
 

## dependencies for log4j ( on spark ) 

The spark runtime uses log4j only and we cannot use log4j2



In a spark context,  log4j is already embedded, all you need is :
log4j_eventhub_1_0_SNAPSHOT.jar ( the component in the subproject log4j )
maven jar org.json:json:20180813
maven jar com.microsoft.azure:azure-eventhubs:2.3.2


if using com.microsoft.azure:azure-eventhubs-spark_2.11:2.3.13 for spark streaming from or to  eventhubs you already have 
com.microsoft.azure:azure-eventhubs:2.3.2 by transitivity


## usage 

```
import org.apache.log4j.Logger;

public class Log4jTest {
	public static org.apache.log4j.Logger log4jLogger = org.apache.log4j.Logger
			.getLogger(Log4jTest.class);

//your code
    static void run() {
        ...
    }

	public static void main(String... argv) throws Exception {
        
        //this is an example
        //in your production code, load the config from an external file.
		Log4jLogs.simpleLogging("{   \"debug\": false,   \"name\" : \"rtfld-rc-ehubns-we-hp\",   \"connectionString\": \"Endpoint=sb://<eventhubnmespace>.servicebus.windows.net/;SharedAccessKeyName=xxxxxx;SharedAccessKey=yyyyyyyyyyyyyyyyyyyyyy=;EntityPath=<eventhub>\",   \"globalFields\": {   \"branch\" : \"TGS/TGITS\",    \"index\" :\"Dataplatform\",    \"application\" : \"RTFLD\",    \"environment\" : \"hp\",    \"source_component\" : \"RTFLD-StreamIoT\",    \"source_name\" :\"RTFLD-StreamIoT\"    } }");

		try {
            run();
		} catch (Throwable t) {
			log4jLogger.error("uncaught exception at top level ", t);
			throw t;
		} finally {
			// this call is mandatory to close everything right
			Log4jLogs.shutdown();
		}
	}
```



## pyspark usage

in pyspark we also use the  log4j loggers

```

#get the helper class com.hightechzone.eventhub.log4j.Log4jLogs and call java static  method simpleLogging
log4jlogs = sc._jvm.com.hightechzone.eventhub.log4j.Log4jLogs
conf = dbutils.widgets.get("eventhubLoggingConfig")
log4jlogs.simpleLogging(conf)

#get a program logger object and set level to INFO ( and WARN for the others )
log4jLogger = sc._jvm.org.apache.log4j
LOGGER = log4jLogger.LogManager.getLogger(__name__)
sc.setLogLevel("WARN")
LOGGER.setLevel( log4jLogger.Level.INFO )


#log as usual calling the logger methods
LOGGER.debug( "simple debug message" )

#( unfortunally parametrized messages are not easy in pyspark )
LOGGER.info( "fake parametrized message : loaded offsets %s" % (str(offsetsData)) )



```


here the configuration is oftained through the databricks job parameters, defined like this :

```
{
  "eventhubLoggingConfig": "{   \"true\": false,   \"name\" : \"<eventhubnamespace>\",   \"connectionString\": \"Endpoint=sb://<eventhubnamespace>.windows.net/;SharedAccessKeyName=xxxxxxxxxxx;SharedAccessKey=yyyyyyyyyyyyyyyyyyyyyy=;EntityPath=<eventhub>\",   \"globalFields\": {   \"branch\" : \"tgs\",    \"index\" :\"dataplatform\",    \"application\" : \"RTFLD\",    \"environment\" : \"PROD\",    \"source_component\" : \"Databricks\",    \"source_name\" :\"RTFLD-StreamIoT\"    } }"
}
```

the parameter name is eventhubLoggingConfig and the value is a string containing json, thus escaped a lot with backslashes