package com.hightechzone.eventhub.jul;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

  import com.microsoft.azure.eventhubs.EventHubClient;
 
public class JulLogs {
	public static Logger logger = Logger.getLogger(JulLogs.class.getName());

	static Formatter jsonFormatter = new JsonFormatter();
	static Formatter simpleFormatter = new SimpleFormatter();

	static EventHubsHandler evtAppender = null;
	static public boolean eventhubEnabled = true;

	private static boolean silenceMicrosoft = true;
	public void dontSilenceMicrosoftlogs() {
		silenceMicrosoft = false;

	}

     public static void simpleLogging() {
        // log4j logging
        LogManager.getLogManager().reset();

         Handler myhandler = new StreamHandler(System.out, simpleFormatter)  {
        	    @Override
        	    public synchronized void publish(LogRecord record) {
        	    	super.publish(record);
        	    	super.flush();
        	    }

         };   
         myhandler.setLevel(Level.INFO);
        if ( silenceMicrosoft )
        	setToLevel("com.microsoft.azure", Level.WARNING);

        myhandler.setFormatter(simpleFormatter);
        Logger jvmmonloggerbase = Logger.getLogger("");
        Handler handlers [] = jvmmonloggerbase.getHandlers();
        if ( handlers != null) {
        	for ( Handler h : handlers) {
        		logger.removeHandler(h);
        	}
        }
        jvmmonloggerbase.addHandler(myhandler);
		if (eventhubEnabled) {
			ConfigObj eventHubConf;
			try {
				eventHubConf = loadEventHubProperties();
				if ( evtAppender  == null) {
					  evtAppender = EventHubsHandler.createHandler(eventHubConf.name,  null, jsonFormatter  ,  eventHubConf.ignoreExceptions, eventHubConf.connectionString,
					  eventHubConf.immediateFlush);
					  evtAppender.setLevel(Level.INFO);
					  evtAppender.start();
					}

			} catch ( Exception e) {
				System.err.println("Caught and ignored exception. No event hub logging in place."+e.toString());
			}
	        jvmmonloggerbase.addHandler(evtAppender);
  		}

		
        jvmmonloggerbase.setUseParentHandlers(false);

    }

	private static ConfigObj loadEventHubProperties()   {
		ConfigObj ret = new ConfigObj();
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("eventhubconfig.properties");
		if (is == null)
			throw new IllegalStateException("eventhubconfig.properties not in classpath ");
		try {
			Properties props = new Properties();
			props.load(is);
			if (props.containsKey("eventhub.name"))
				ret.name = props.getProperty("eventhub.name");
			if (props.containsKey("eventhub.connectionString"))
				ret.ignoreExceptions = Boolean.parseBoolean(props.getProperty("eventhub.ignoreExceptions"));
			if (props.containsKey("eventhub.connectionString"))
				ret.connectionString = props.getProperty("eventhub.connectionString");
			if (props.containsKey("eventhub.immediateFlush"))
				ret.immediateFlush = Boolean.parseBoolean(props.getProperty("eventhub.immediateFlush"));
		} catch (Exception e) {
			throw  new IllegalStateException (e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

    static Logger getLogger(String name) {
        Logger targetLogger = Logger.getLogger("");
        if (name != null && !name.isEmpty())
            targetLogger = Logger.getLogger(name);
        return targetLogger;
    }

    public static void setToLevel(String name, String level) {
        Level targetLogLev = Level.parse(level);
        setToLevel(name, targetLogLev);

    }

    public static void setToLevel(String name, Level lev) {
        Logger targetLogger = getLogger(name);
        targetLogger.setLevel(lev);

    }

    public static void ensureLevel(String name, Level lev) {
        Logger targetLogger = getLogger(name);
        int targetLogLev = lev.intValue();
        if (targetLogger.getLevel() != null && targetLogger.getLevel().intValue() < targetLogLev) {
            logger.warning("downgrading to level " + lev + " instead of " + targetLogger.getLevel() + " for logger " + name);
        }
        targetLogger.setLevel(lev);
        while (targetLogger != null) {
            Handler[] handlers = targetLogger.getHandlers();
            if (handlers != null) {
                for (Handler h : handlers) {
                    if (h.getLevel().intValue() > targetLogLev) {
                        logger.warning("restauring level " + lev + " for handler " + h.getClass().getName());
                        h.setLevel(lev);
                    }

                }
            }
            targetLogger = targetLogger.getParent();
        }
    }

    public static void ensureWarning(String name) {
        ensureLevel(name, Level.WARNING);
    }

    public static void setDebug(String name) {
        setToLevel(name, Level.FINEST);
    }

    public static void setDebug() {
        setDebug("");
    }

	public static void shutdown() throws Exception {
        LogManager.getLogManager().reset();
		
	}

 
}
