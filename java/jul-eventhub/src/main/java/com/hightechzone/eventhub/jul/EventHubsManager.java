// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package com.hightechzone.eventhub.jul;

import com.microsoft.azure.eventhubs.EventData;
import com.microsoft.azure.eventhubs.EventHubClient;
import com.microsoft.azure.eventhubs.EventHubException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public final class EventHubsManager {
	private   ScheduledExecutorService EXECUTOR_SERVICE = null;
	private final String eventHubConnectionString;
	private EventHubClient eventHubSender;
	private String name;

	protected EventHubsManager(final String name, final String eventHubConnectionString) {
		this.name = name;
		this.eventHubConnectionString = eventHubConnectionString;
	}

	public void send(final byte[] msg) throws EventHubException {
		if (msg != null) {
			EventData data = EventData.create(msg);
			this.eventHubSender.sendSync(data);
		}
	}

	public synchronized void send(final ConcurrentLinkedQueue<byte[]> messages) throws EventHubException {
		if (messages != null) {
			LinkedList<EventData> events = new LinkedList<EventData>();
			byte[] data = messages.poll();
			while (data != null) {
				events.add(EventData.create(data));
				data = messages.poll();
			}
			this.eventHubSender.sendSync(events);
		}
	}

	public void startup() throws EventHubException, IOException {
		EXECUTOR_SERVICE = Executors.newScheduledThreadPool(1);
		this.eventHubSender = EventHubClient.createSync(this.eventHubConnectionString, EXECUTOR_SERVICE);
	}

	public void shutdown() {

		// not enough : a bug doesnt close the clienteventhub thread pool

		if (eventHubSender != null) {
			try {
				eventHubSender.closeSync();
			} catch (Exception e) {
				System.out.println("caught exception on close " + e.toString());

			}
			eventHubSender = null;
		}
		if (EXECUTOR_SERVICE != null) {
			try {
				EXECUTOR_SERVICE.shutdownNow();
			} catch (Exception e) {
				System.out.println("caught exception on shutdownNow " + e.toString());

			}
			EXECUTOR_SERVICE = null;
		}


	}

}
