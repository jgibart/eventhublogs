// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package com.hightechzone.eventhub.jul;


import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

 
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Sends {@link LogEvent}'s to Microsoft Azure EventHubs. By default, tuned for
 * high performance and hence, pushes a batch of Events.
 */
public final class EventHubsHandler extends Handler  {
	private static final int MAX_BATCH_SIZE_BYTES = 8 * 1024;

	// this constant is tuned to use the MaximumAllowedMessageSize(256K) including
	// AMQP-Headers for a LogEvent of 1Char
	private static final int secondsTillSend = 5;
	private static final int MAX_BATCH_SIZE = 21312;

	private static final long serialVersionUID = 1L;

	private final EventHubsManager eventHubsManager;
	private final boolean immediateFlush;
	private final AtomicInteger currentBufferedSizeBytes;
	private final ConcurrentLinkedQueue<byte[]> logEvents;

	private final String name;

	private final Filter filter;

	private final Formatter layout;

	private static boolean noticeDisplayed = false;
	Thread flusherThread = null;

	public void disableNotice() {
		noticeDisplayed = true;
	}
	
	private EventHubsHandler(final String name,        Filter filter ,    Formatter layout ,

			final boolean ignoreExceptions, final EventHubsManager eventHubsManager, final boolean immediateFlush) {
		super();

		this.name = name;
		this.eventHubsManager = eventHubsManager;
		this.filter = filter;
		this.layout = layout;
		this.immediateFlush = immediateFlush;
		this.logEvents = new ConcurrentLinkedQueue<byte[]>();
		this.currentBufferedSizeBytes = new AtomicInteger();
		
	}

	long nextFlush = System.currentTimeMillis()+1000*secondsTillSend;
	protected long getDelayTillFlush(long current) {
 		return  Math.max(nextFlush - current  , 0);
	}

	private boolean mustExit( ) {
		 
		return flusherThread == null;
	}

	private void maybeFlush(long current) {
		if ( current < nextFlush) 
			return;
		flush();
	}

 	public static EventHubsHandler createHandler(
		 final String name,    
		 Filter filter , 
		 Formatter layout ,
		  final boolean ignoreExceptions,
	 final String connectionString,
			 final boolean immediateFlush) {
		final EventHubsManager eventHubsManager = new EventHubsManager(name, connectionString);
		return new EventHubsHandler(name, filter,layout , ignoreExceptions, eventHubsManager, immediateFlush);
	}

	@Override
	public void publish(LogRecord record) {
		byte[] serializedLogEvent = null;
		if  ( ! noticeDisplayed) {
			
			noticeDisplayed = true;
			System.err.println("Logging to event hub "+getName()+" in place.");
		}
		try {
			Formatter  layout = getLayout();

 			serializedLogEvent = layout.format(record).getBytes(StandardCharsets.UTF_8);
			 

			if (serializedLogEvent != null) {
				if (this.immediateFlush) {
					this.eventHubsManager.send(serializedLogEvent);
					return;
				} else {
					int currentSize = this.currentBufferedSizeBytes.addAndGet(serializedLogEvent.length);
					this.logEvents.offer(serializedLogEvent);
					int count = this.logEvents.size();
					if (currentSize < MAX_BATCH_SIZE_BYTES
							&& this.logEvents.size() < MAX_BATCH_SIZE && record.getMillis() < nextFlush ) {
						return;
					}

 
 					this.eventHubsManager.send(this.logEvents);
 
					this.currentBufferedSizeBytes.set(0);
					nextFlush  = System.currentTimeMillis() +1000*secondsTillSend;

				}
			}
		} catch (final Throwable exception) {
 
			System.err.println(String.format(Locale.US, "[%s] Appender failed to logEvent to EventHub.", this.name));

			// remove the current LogEvent from the inMem Q - to avoid replay
			if (serializedLogEvent != null && this.logEvents.remove(serializedLogEvent)) {
				this.currentBufferedSizeBytes.addAndGet(-1 * serializedLogEvent.length);
			}

			throw new IllegalStateException(exception);
		}
		
	}
 

 	public void start() {
 
		try {
			this.eventHubsManager.startup();

			if (!immediateFlush) {
				flusherThread = new Thread(new Runnable() {

					@Override
					public void run() {
						synchronized (flusherThread) {
							try {
								long current = System.currentTimeMillis();
								while (!mustExit( )) {
									long waitTime = getDelayTillFlush(current);
									if (waitTime > 0) {
										flusherThread.wait(waitTime);
									}
									 current = System.currentTimeMillis();
									maybeFlush(current);

								}
							} catch (InterruptedException e) {
							}
						}
					}

				}, "logs flushing thread");
				flusherThread.setDaemon(true);
				flusherThread.start();
			}
		} catch (Throwable exception) {
			final String errMsg = String.format(Locale.US, "[%s] Appender initialization failed with error: [%s]",
					this.getName(), exception.getMessage());

			System.err.println(errMsg);
			throw new IllegalStateException(errMsg, exception);
		}
	}

	public void flush ( ) {
		if (this.immediateFlush) {
			return;
		}
		if (this.logEvents.size() == 0) {
			return;
		}

		try {
 
			this.eventHubsManager.send(this.logEvents);
			this.currentBufferedSizeBytes.set(0);
			nextFlush  = System.currentTimeMillis() +1000*secondsTillSend;

		} catch (final Throwable exception) {
 
			System.err.println(String.format(Locale.US, "[%s] Appender failed to logEvent to EventHub.", this.getName()));

			// remove the current LogEvent from the inMem Q - to avoid replay

		}

	}
	

	public String getName() {
		return name;
	}

	private Formatter getLayout() {
		return layout;
	}

	@Override
	public void close() throws SecurityException {
		flusherThread = null;
		flush();
		this.eventHubsManager.shutdown();
 	}

}
