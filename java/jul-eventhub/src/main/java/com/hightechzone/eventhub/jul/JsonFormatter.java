package com.hightechzone.eventhub.jul;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public final class JsonFormatter extends Formatter {

 
    static ThreadLocal<SimpleDateFormat> fmt = new ThreadLocal<SimpleDateFormat>() {
        protected SimpleDateFormat initialValue() {
        	//SimpleDateFormat  is not htread safe. Here is the cure
        	SimpleDateFormat ret =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        	ret.setTimeZone(TimeZone.getDefault());
        	return ret;
        }

    };
 
    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        sb.append("{ \"date\" : \"");
        sb.append(fmt.get().format(new Date(record.getMillis())));
        sb.append("\",  \"level\": \"");
        sb.append(record.getLevel().toString());
        sb.append("\",  \"sequence\":\"");
        sb.append(record.getSequenceNumber());
        sb.append("\",  \"loggerName\": \"");
        sb.append( escapeJson(record.getLoggerName().replaceAll(".*\\.", "")));
        sb.append("\",  \"threadId\": \"");
        sb.append( record.getThreadID());
        sb.append("\",  \"message\": \"");
        sb.append(escapeJson(record.getMessage()));
        sb.append("\"");
        if (record.getThrown() != null) {
            sb.append(",  \"exception\": \"");
            sb.append(escapeJson(record.getThrown().toString()));
            sb.append("\"");
        }
        sb.append(" }\n");

        return sb.toString();
    }
     
	private Object escapeJson(String txt) {

		IntPredicate predicate = new IntPredicate() {
			
			@Override
			public boolean test(int value) {
				return value == '\b' || value == '\f' || value == '\n' || value == '\r' ||value == '\t' ||value == '"' ||value == '\\'  ;
			}
		};
//	    Backspace is replaced with \b
//	    Form feed is replaced with \f
//	    Newline is replaced with \n
//	    Carriage return is replaced with \r
//	    Tab is replaced with \t
//	    Double quote is replaced with \"
//	    Backslash is replaced with \\
		boolean hasSpecial = txt.chars().anyMatch(predicate );
		if ( ! hasSpecial )
			return txt;
		StringBuilder escaped = new StringBuilder();
		 txt.chars().forEach(new IntConsumer() {
			
			@Override
			public void accept(int value) {
				switch (value) {
					case '\b':
						escaped.append("\\b");
						break;
					case '\f':
						escaped.append("\\f");
						break;
					case '\n':
						escaped.append("\\n");
						break;
					case '\r':
						escaped.append("\\r");
						break;
					case '\t':
						escaped.append("\\t");
						break;
					case '"':
						escaped.append("\\\"");
						break;
					case '\\':
						escaped.append("\\\\");
						break;
					default:
							escaped.append(Character.valueOf((char) value));
							
				}
				
			}
		});
	    return escaped.toString();
	}
}
