package com.hightechzone.eventhub.jul;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public final class SimpleFormatter extends Formatter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");


    
    static ThreadLocal<SimpleDateFormat> fmt = new ThreadLocal<SimpleDateFormat>() {
        protected SimpleDateFormat initialValue() {
        	//SimpleDateFormat  is not htread safe. Here is the cure
        	SimpleDateFormat ret =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        	ret.setTimeZone(TimeZone.getDefault());
        	return ret;
        }

    };
 
    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        sb.append(fmt.get().format(new Date(record.getMillis())));
        sb.append(" - [");
        sb.append(record.getLevel().toString());
        sb.append("] <");
        sb.append(record.getLoggerName().replaceAll(".*\\.", ""));
        sb.append("> {");
        sb.append(record.getThreadID());
        sb.append("} ");
        sb.append(record.getMessage());
        sb.append(LINE_SEPARATOR);

        if (record.getThrown() != null) {
            try {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                record.getThrown().printStackTrace(pw);
                pw.close();
                sb.append(sw.toString());
            } catch (Exception ex) {
                // ignore
            }
        }

        return sb.toString();
    }
}
