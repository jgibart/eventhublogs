package com.hightechzone.eventhub.jul.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.URI;
import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.hightechzone.eventhub.jul.JulLogs;

public class JulTest {
	public static Logger slf4jLogger = LoggerFactory.getLogger(JulTest.class);
	public static java.util.logging.Logger julLogger = java.util.logging.Logger.getLogger(JulTest.class.getName());
	@Parameter(names = { "-d", "--debug" }, description = "Debug mode")
	boolean debug = false;

	public static void main(String... argv) throws Exception {
		JulLogs.simpleLogging();
		JulTest main = new JulTest();
		JCommander.newBuilder().addObject(main).build().parse(argv);
		try {

			main.run();
		} catch (Throwable t) {
			slf4jLogger.error("uncaught exception at top level ", t);
			throw t;
		} finally {
			// this call is mandatory to close everything right
			JulLogs.shutdown();
		}

	}

	public void run() throws Exception {
		if (debug)
			JulLogs.setDebug();

		slf4jLogger.trace("This is a SLF4j -> JUL log trace line");
		slf4jLogger.debug("This is a SLF4j -> JUL log debug line");
		slf4jLogger.info("This is a SLF4j -> JUL log info line");
		slf4jLogger.warn("This is a SLF4j -> JUL log warning line");
		slf4jLogger.error("This is a SLF4j -> JUL log error line");

		Exception sampleException = new ConnectException("sample connect exception");
		slf4jLogger.trace("This is a SLF4j -> JUL log trace line with exception", sampleException);
		slf4jLogger.debug("This is a SLF4j -> JUL log debug line with exception", sampleException);
		slf4jLogger.info("This is a SLF4j -> JUL log info line with exception", sampleException);
		slf4jLogger.warn("This is a SLF4j -> JUL log warning line with exception", sampleException);
		slf4jLogger.error("This is a SLF4j -> JUL log error line with exception", sampleException);

		julLogger.finest("This is a raw JUL log finest line");
		julLogger.finer("This is a raw JUL log finer line");
		julLogger.fine("This is a raw JUL log fine line");
		julLogger.info("This is a raw JUL log info line");
		julLogger.warning("This is a raw JUL log warning line");
		julLogger.severe("This is a raw JUL log error line");

		julLogger.log(Level.FINEST, "This is a raw JUL log fineest line with exception", sampleException);
		julLogger.log(Level.FINER, "This is a raw JUL log finer line with exception", sampleException);
		julLogger.log(Level.FINE, "This is a raw JUL log fine line with exception", sampleException);
		julLogger.log(Level.INFO, "This is a raw JUL log info line with exception", sampleException);
		julLogger.log(Level.WARNING, "This is a raw JUL log warning line with exception", sampleException);
		julLogger.log(Level.SEVERE, "This is a raw JUL log error line with exception", sampleException);


		  for ( int i = 0; i < 10; i++) {
			  slf4jLogger.info("This is a slow log");
			  Thread.sleep(1000);
		  }
		  

	}

}
