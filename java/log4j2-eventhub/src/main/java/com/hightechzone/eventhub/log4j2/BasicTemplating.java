package com.hightechzone.eventhub.log4j2;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BasicTemplating {



        static Pattern varPattern = Pattern.compile("(\\$\\{([^\\}]*)\\})");


       public static String getInterPolatedString(String orig) {
            String ret = orig;
            boolean redo = false;
            do {
                StringBuffer sb = new StringBuffer();
                redo = false;
                Matcher m = varPattern.matcher(ret);
                while (m.find()) {
                    redo = true;
                    String varName = m.group(2);
                    String replacement = getInterPolatedValue(varName);
                    m.appendReplacement(sb, Matcher.quoteReplacement(replacement));
                }
                m.appendTail(sb);
                ret = sb.toString();
            } while (redo);
            return ret;

        }
        static Date currentDate = new Date();
        static String getInterPolatedValue(String keyName) {
            if (keyName.startsWith("sysprop:") ) {
                String varName = keyName.substring("sysprop:".length());
                String ret = System.getProperty(varName);
                if ( ret == null)
                    throw new IllegalStateException("Missing system property  "+varName);
                return ret;
            }
            else if (keyName.startsWith("env:") ) {
                String varName = keyName.substring("env:".length());
                String ret =  System.getenv(varName);
                if ( ret == null)
                    throw new IllegalStateException("Missing environment variable   "+varName);
                return ret;


            } else         if (keyName.startsWith("SimpleDateFormat:") ) {
                String varName =keyName.substring("SimpleDateFormat:".length());
                SimpleDateFormat sdf = new SimpleDateFormat(varName);
                String ret = sdf.format(currentDate);
                return ret;

            }



            throw new IllegalStateException("variable name not recognized "+keyName);
        }

    }
