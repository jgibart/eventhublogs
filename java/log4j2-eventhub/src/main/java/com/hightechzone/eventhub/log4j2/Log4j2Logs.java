package com.hightechzone.eventhub.log4j2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.LoggerConfig;

import com.microsoft.azure.eventhubs.EventHubClient;

public class Log4j2Logs {
	public static Logger logger = LogManager.getLogger(Log4j2Logs.class);

	private static boolean silenceMicrosoft = true;

	public void dontSilenceMicrosoftlogs() {
		silenceMicrosoft = false;

	}

	static EventHubsAppender evtAppender = null;
	static public boolean eventhubEnabled = true;

	public static void simpleLogging() throws Exception {
		// log4j logging
		final LoggerContext ctx = (LoggerContext) LogManager.getContext(true);
		if ( !ctx.isStarted()) {
			ctx.start();
		}

		System.setProperty("log4j2.isThreadContextMapInheritable", "true");
		setToLevel("", Level.INFO);
		if (silenceMicrosoft)
			setToLevel("com.microsoft.azure", Level.WARN);

		if (eventhubEnabled) {
			ConfigObj eventHubConf = loadEventHubProperties();
			final Configuration config = ctx.getConfiguration();
			if (eventHubConf.layout == null) {
				eventHubConf.layout = JsonLayout.createLayout(config, StandardCharsets.UTF_8);
			}
			if (evtAppender == null) {
				evtAppender = EventHubsAppender.createAppender(eventHubConf.name, eventHubConf.filter,
						eventHubConf.layout, eventHubConf.ignoreExceptions, eventHubConf.connectionString,
						eventHubConf.immediateFlush, eventHubConf.debug);
				config.addAppender(evtAppender);
				ctx.getRootLogger().addAppender(ctx.getConfiguration().getAppender(evtAppender.getName()));
			}
			evtAppender.start();
			ctx.updateLoggers();
		}

	}

	private static ConfigObj loadEventHubProperties() throws IOException {
		ConfigObj ret = new ConfigObj();
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("eventhubconfig.properties");
		if (is == null)
			throw new IllegalStateException("eventhubconfig.properties not in classpath ");
		try {
			Properties props = new Properties();
			props.load(is);
			if (props.containsKey("eventhub.name"))
				ret.name = BasicTemplating.getInterPolatedString(props.getProperty("eventhub.name"));
			if (props.containsKey("eventhub.debug"))
				ret.debug = Boolean.parseBoolean(props.getProperty("eventhub.debug"));
			if (props.containsKey("eventhub.connectionString"))
				ret.ignoreExceptions = Boolean.parseBoolean(props.getProperty("eventhub.ignoreExceptions"));
			if (props.containsKey("eventhub.connectionString"))
				ret.connectionString = BasicTemplating
						.getInterPolatedString(props.getProperty("eventhub.connectionString"));
			if (props.containsKey("eventhub.immediateFlush"))
				ret.immediateFlush = Boolean.parseBoolean(props.getProperty("eventhub.immediateFlush"));
			for (Entry<Object, Object> ent : props.entrySet()) {
				String key = (String) ent.getKey();
				if (key.startsWith("eventhub.globalFields.")) {
					ThreadContext.put(key.substring("eventhub.globalFields.".length()),
							BasicTemplating.getInterPolatedString((String) ent.getValue()));
				}
			}
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	static Logger getLogger(String name) {
		Logger targetLogger = LogManager.getRootLogger();
		if (name != null && !name.isEmpty())
			targetLogger = LogManager.getLogger(name);
		return targetLogger;
	}

	public static void setToLevel(String name, Level lev) {
		Configurator.setLevel(name, lev);
	}

	public static void ensureLevel(String name, Level lev) {
		Logger targetLogger = getLogger(name);
		int targetLogLev = lev.intLevel();
		if (targetLogger.getLevel() != null && targetLogger.getLevel().intLevel() < targetLogLev) {
			logger.warn(
					"downgrading to level " + lev + " instead of " + targetLogger.getLevel() + " for logger " + name);
		}
		Configurator.setLevel(name, lev);

	}

	public static void ensureWarning(String name) {
		ensureLevel(name, Level.WARN);
	}

	public static void setDebug(String name) {
		setToLevel(name, Level.DEBUG);
	}

	public static void setDebug() {
		setDebug("");
	}

	public static void shutdown() throws Exception {
		final LoggerContext ctx = (LoggerContext) LogManager.getContext(true);

		LogManager.shutdown(ctx);
		if (evtAppender != null) {
			ctx.getRootLogger().removeAppender(evtAppender);
			ctx.reconfigure();
			evtAppender = null;
		}

	}

	/**
	 * method responsible to add the email appender to a specific logger.
	 *
	 * @param name
	 * @param alerter
	 */

}
