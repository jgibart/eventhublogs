package com.hightechzone.eventhub.log4j2;

import java.io.Serializable;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;

 
public class ConfigObj {
	  String name = null;
      Filter filter = null;
      Layout<? extends Serializable> layout = null;
      boolean ignoreExceptions= false;
      String connectionString = null;
      boolean immediateFlush = false;
      boolean debug = false;
}
