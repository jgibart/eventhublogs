package com.hightechzone.eventhub.log4j2;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;

import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginConfiguration;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;
import org.apache.logging.log4j.core.layout.ByteBufferDestination;
import org.apache.logging.log4j.util.ReadOnlyStringMap;

@Plugin(name = "CustomConsoleLayout", category = "azure", elementType = Layout.ELEMENT_TYPE)
public class JsonLayout extends AbstractStringLayout  {
    static final String CONTENT_TYPE = "application/json";
    static final Charset charset = StandardCharsets.UTF_8;
    AtomicLong counter = new AtomicLong( );

    static ThreadLocal<SimpleDateFormat> fmt = new ThreadLocal<SimpleDateFormat>() {
        protected SimpleDateFormat initialValue() {
        	//SimpleDateFormat  is not htread safe. Here is the cure
        	SimpleDateFormat ret =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        	ret.setTimeZone(TimeZone.getDefault());
        	return ret;
        }

    };
    
    public JsonLayout(Configuration config, Charset aCharset ) {
        super(config, aCharset, null, null);
     }
    static class ContextSerializerCtx{
    	final StringBuilder sb;
    	int index  = 0;
    	ContextSerializerCtx(final StringBuilder sb){
    		this.sb = sb;
    	}
    }
    //                        "logger_name": record.name, "log_message" : record.message}

	private String toJsonString(LogEvent record) {
        StringBuilder sb = new StringBuilder();
        sb.append("{ \"timestamp\" : \"");
        sb.append(fmt.get().format(new Date(record.getTimeMillis())));
        sb.append("\",  \"criticality\": \"");
        sb.append(record.getLevel().toString());
//        sb.append("\",  \"sequence\":\"");
//        sb.append(counter.incrementAndGet());
        sb.append("\",  \"logger_name\": \"");
        sb.append( escapeJson(record.getLoggerName().replaceAll(".*\\.", "")));
        sb.append("\",  \"log_message\": \"");
        String formatted = record.getMessage().getFormattedMessage();
        sb.append(escapeJson(formatted ));
        if ( !formatted .equals(record.getMessage().getFormat())) {
            sb.append("\",  \"message_format\": \"");
            sb.append(escapeJson(record.getMessage().getFormat()));        	
        }
        sb.append("\"");
        Object[] params =record.getMessage().getParameters() ;
        if  (params!= null && params.length > 0) {
            sb.append(",  \"message_args\": {");
            for ( int i = 0; i < params.length ; i++) {
            	if ( i != 0)
                    sb.append(", ");
                sb.append("\"arg"+String.valueOf( i)+"\": \"");
                sb.append(escapeJson(params[i].toString()));        	
                sb.append("\"");
            	
            }
            sb.append("}");
        	
        }
        ReadOnlyStringMap contextData = record.getContextData();
        if ( contextData != null && contextData.size() > 0) {
        	final ContextSerializerCtx ctx = new ContextSerializerCtx( sb);
            sb.append(", ");
            contextData.forEach((key, value) -> {
            	if ( ctx.index !=0)
                    sb.append(", ");
            	ctx.index++;
                sb.append("\""+key+"\": \"");
                sb.append(escapeJson(value.toString()));        	
                sb.append("\"");
            	
            	
            	
            }); 
            
        	
        	
        }
        if (record.getThrown() != null ) {
            sb.append(",  \"exception\": \"");
            sb.append(escapeJson(record.getThrown().toString()));
            sb.append("\"");
        }
        sb.append(" }\n");
        
        return sb.toString();
	}
 

	private Object escapeJson(String txt) {

		IntPredicate predicate = new IntPredicate() {
			
			@Override
			public boolean test(int value) {
				return value == '\b' || value == '\f' || value == '\n' || value == '\r' ||value == '\t' ||value == '"' ||value == '\\'  ;
			}
		};
//	    Backspace is replaced with \b
//	    Form feed is replaced with \f
//	    Newline is replaced with \n
//	    Carriage return is replaced with \r
//	    Tab is replaced with \t
//	    Double quote is replaced with \"
//	    Backslash is replaced with \\
		boolean hasSpecial = txt.chars().anyMatch(predicate );
		if ( ! hasSpecial )
			return txt;
		StringBuilder escaped = new StringBuilder();
		 txt.chars().forEach(new IntConsumer() {
			
			@Override
			public void accept(int value) {
				switch (value) {
					case '\b':
						escaped.append("\\b");
						break;
					case '\f':
						escaped.append("\\f");
						break;
					case '\n':
						escaped.append("\\n");
						break;
					case '\r':
						escaped.append("\\r");
						break;
					case '\t':
						escaped.append("\\t");
						break;
					case '"':
						escaped.append("\\\"");
						break;
					case '\\':
						escaped.append("\\\\");
						break;
					default:
							escaped.append(Character.valueOf((char) value));
							
				}
				
			}
		});
	    return escaped.toString();
	}
    @PluginFactory
    public static JsonLayout createLayout(@PluginConfiguration final Configuration config,
          @PluginAttribute(value = "charset", defaultString = "UTF-8") final Charset charset  
                                                  ) {
        return new JsonLayout(config, charset);
    }
	@Override
	public String toSerializable(LogEvent event) {
		return toJsonString(event);
	}


}
