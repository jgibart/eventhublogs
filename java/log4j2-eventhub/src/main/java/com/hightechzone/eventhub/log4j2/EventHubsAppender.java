// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package com.hightechzone.eventhub.log4j2;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LifeCycle;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.apache.logging.log4j.core.util.StringEncoder;

import java.io.Serializable;

import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Sends {@link LogEvent}'s to Microsoft Azure EventHubs. By default, tuned for
 * high performance and hence, pushes a batch of Events.
 */
@Plugin(name = "EventHub", category = "Core", elementType = "appender", printObject = true)
public final class EventHubsAppender extends AbstractAppender {
	private static final int MAX_BATCH_SIZE_BYTES = 8 * 1024;

	// this constant is tuned to use the MaximumAllowedMessageSize(256K) including
	// AMQP-Headers for a LogEvent of 1Char
	private static final int secondsTillSend = 5;
	private static final int MAX_BATCH_SIZE = 21312;
	private static final long serialVersionUID = 1L;

	private final EventHubsManager eventHubsManager;
	private final boolean immediateFlush;
	private final boolean debug;
	private final AtomicInteger currentBufferedSizeBytes;
	private final ConcurrentLinkedQueue<byte[]> logEvents;

	private static boolean noticeDisplayed = false;
	FlusherThread flusherThread = null;

	public void disableNotice() {
		noticeDisplayed = true;
	}

	private EventHubsAppender(final String name, final Filter filter, final Layout<? extends Serializable> layout,
			final boolean ignoreExceptions, final EventHubsManager eventHubsManager, final boolean immediateFlush,
			final boolean debug) {
		super(name, filter, layout, ignoreExceptions);

		this.eventHubsManager = eventHubsManager;
		this.immediateFlush = immediateFlush;
		this.debug = debug;
		this.logEvents = new ConcurrentLinkedQueue<byte[]>();
		this.currentBufferedSizeBytes = new AtomicInteger();

	}

	long nextFlush = System.currentTimeMillis() + 1000 * secondsTillSend;

	protected long getDelayTillFlush(long current) {
		return Math.max(nextFlush - current, 0);
	}

	private void maybeFlush(long current) {
		if (current < nextFlush)
			return;
		flush();
	}

	@PluginFactory
	public static EventHubsAppender createAppender(
			@Required(message = "Provide a Name for EventHubs Log4j Appender") @PluginAttribute("name") final String name,
			@PluginElement("Filter") final Filter filter,
			@PluginElement("Layout") final Layout<? extends Serializable> layout,
			@PluginAttribute(value = "ignoreExceptions", defaultBoolean = true) final boolean ignoreExceptions,
			@Required(message = "Provide EventHub connection string to append the events to") @PluginAttribute("eventHubConnectionString") final String connectionString,
			@PluginAttribute(value = "immediateFlush", defaultBoolean = false) final boolean immediateFlush,
			@PluginAttribute(value = "debug", defaultBoolean = false) final boolean debug) {
		final EventHubsManager eventHubsManager = new EventHubsManager(name, connectionString, debug);
		return new EventHubsAppender(name, filter, layout, ignoreExceptions, eventHubsManager, immediateFlush, debug);
	}

	@Override
	public void append(LogEvent logEvent) {
		byte[] serializedLogEvent = null;
		if (!noticeDisplayed) {

			noticeDisplayed = true;
			System.err.println("Logging to event hub " + getName() + " in place.");
		}
		try {
			Layout<? extends Serializable> layout = getLayout();

			if (layout != null) {
				serializedLogEvent = layout.toByteArray(logEvent);
			} else {
				serializedLogEvent = StringEncoder.toBytes(logEvent.getMessage().getFormattedMessage(), UTF_8);
			}

			if (serializedLogEvent != null) {
				if (this.immediateFlush) {
					this.eventHubsManager.send(serializedLogEvent);
					return;
				} else {
					nextFlush = System.currentTimeMillis() + 1000 * secondsTillSend;
					int currentSize = this.currentBufferedSizeBytes.addAndGet(serializedLogEvent.length);
					this.logEvents.offer(serializedLogEvent);
					int count = this.logEvents.size();
					if (currentSize < EventHubsAppender.MAX_BATCH_SIZE_BYTES
							&& this.logEvents.size() < EventHubsAppender.MAX_BATCH_SIZE
							&& logEvent.getTimeMillis() < nextFlush && !logEvent.isEndOfBatch()) {
						return;
					}

					this.eventHubsManager.send(this.currentBufferedSizeBytes, this.logEvents);
				}
			}
		} catch (final Throwable exception) {

			System.err
					.println(String.format(Locale.US, "[%s] Appender failed to logEvent to EventHub.", this.getName()));

			// remove the current LogEvent from the inMem Q - to avoid replay
			exception.printStackTrace(System.err);
		}
	}

	class FlusherThread extends Thread {
		public FlusherThread() {
			super("logs flushing thread");
		}

		public void run() {
			synchronized (this) {
				try {
					long current = System.currentTimeMillis();
					while (!isFinished()) {
						long waitTime = getDelayTillFlush(current);
						if (waitTime > 0) {
							wait(waitTime);
						}
						current = System.currentTimeMillis();
						maybeFlush(current);

					}
				} catch (InterruptedException e) {
				}
			}

		}

		void setFinished() {
			finished = true;
			synchronized (this) {
				notify();
			}

		};

		boolean isFinished() {
			return finished;
		};

		boolean finished = false;
	}

	@Override
	public void start() {
		super.start();

		try {
			this.eventHubsManager.startup();
			if (!immediateFlush) {
				flusherThread = new FlusherThread();
				flusherThread.setDaemon(true);
				flusherThread.start();
			}

		} catch (Throwable exception) {
			final String errMsg = String.format(Locale.US, "[%s] Appender initialization failed with error: [%s]",
					this.getName(), exception.getMessage());

			System.err.println(errMsg);
			exception.printStackTrace(System.err);
			throw new AppenderLoggingException(errMsg, exception);
		}
	}

	private void flush() {
		if (debug) {
			System.out.println("flush called");
		}
		nextFlush = System.currentTimeMillis() + 1000 * secondsTillSend;
		if (this.immediateFlush) {
			return;
		}
		if (this.logEvents.size() == 0) {
			return;
		}

		try {

			this.eventHubsManager.send(this.currentBufferedSizeBytes, this.logEvents);

		} catch (final Throwable exception) {

			System.err
					.println(String.format(Locale.US, "[%s] Appender failed to logEvent to EventHub.", this.getName()));
			exception.printStackTrace(System.err);

			// remove the current LogEvent from the inMem Q - to avoid replay

		}

	}

	@Override
	protected void setStopped() {
		flush();
		if (flusherThread != null) {
			flusherThread.setFinished();
			flusherThread = null;
		}
		this.eventHubsManager.shutdown();
		super.setStopped();

	}

}
