package com.hightechzone.eventhub.log4j2.test;

import java.io.File;
import java.net.ConnectException;
import java.util.Date;

import org.apache.logging.log4j.CloseableThreadContext;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.hightechzone.eventhub.log4j2.Log4j2Logs;

public class Log4j2Test {
	public static Logger slf4jLogger = LoggerFactory.getLogger(Log4j2Test.class);
	public static org.apache.logging.log4j.Logger log42jLogger = org.apache.logging.log4j.LogManager
			.getLogger(Log4j2Test.class);

	@Parameter(names = { "-d", "--debug" }, description = "Debug mode")
	boolean debug = false;

	public static void main(String... argv) throws Exception {
		
	
		Log4j2Logs.simpleLogging();
		Log4j2Test main = new Log4j2Test();
		JCommander.newBuilder().addObject(main).build().parse(argv);
		try {
			if (main.debug)
				Log4j2Logs.setDebug("com.hightechzone");
			main.testDirectLog4j2();
			main.testSlf4j();
		} catch (Throwable t) {
			slf4jLogger.error("uncaught exception at top level ", t);
			throw t;
		} finally {
			// this call is mandatory to close everything right
			Log4j2Logs.shutdown();
		}
	}
	public void testSlf4j() throws Exception {
		MDC.put("username", "jocelyn");
		slf4jLogger.trace("This is a SLF4j -> log4j log trace line");
		slf4jLogger.debug("This is a SLF4j -> log4j log debug line");
		slf4jLogger.info("This is a SLF4j -> log4j log info line");
		slf4jLogger.info("This is a parametrized SLF4j -> log4j  parameter1 {}, parameter2 {}, parameter3 {}",
				new Object[] { "someValue", new File("."), new Date() });
		slf4jLogger.warn("This is a SLF4j -> log4j log warning line");
		slf4jLogger.error("This is a SLF4j -> log4j log error line");
		MDC.remove("username");

		try (CloseableThreadContext.Instance ctx = CloseableThreadContext.put("username", "raymond")) {
			Exception sampleException = new ConnectException("dont panic this is just a fake connect exception");
			slf4jLogger.warn("This is a SLF4j -> log4j log warning line with exception", sampleException);
			slf4jLogger.error("This is a SLF4j -> log4j log error line with exception", sampleException);

		}
		for (int i = 0; i < 10; i++) {
			slf4jLogger.info("This is a slow log with number {}", i);
			Thread.sleep(1000);
		}
	}
	public void testDirectLog4j2() throws Exception {

		ThreadContext.put("username", "jocelyn");
		log42jLogger.trace("This is a SLF4j -> log4j log trace line");
		log42jLogger.debug("This is a SLF4j -> log4j log debug line");
		log42jLogger.info("This is a SLF4j -> log4j log info line");
		log42jLogger.info("This is a parametrized SLF4j -> log4j  parameter1 {}, parameter2 {}, parameter3 {}",
				new Object[] { "someValue", new File("."), new Date() });
		log42jLogger.warn("This is a SLF4j -> log4j log warning line");
		log42jLogger.error("This is a SLF4j -> log4j log error line");
		ThreadContext.remove("username");
		try (CloseableThreadContext.Instance ctx = CloseableThreadContext.put("username", "raymond")) {
			Exception sampleException = new ConnectException("dont panic this is just a fake connect exception");

			log42jLogger.trace("This is a raw log4j log trace line");
			log42jLogger.debug("This is a raw log4j log debug line");
			log42jLogger.info("This is a log4j parametrized info line parameter1 {}, parameter2 {}, parameter3 {}",
					new Object[] { "someValue", new File("."), new Date() });
			log42jLogger.warn("This is a raw log4j log warning line");
			log42jLogger.error("This is a raw log4j log error line");

			log42jLogger.warn("This is a raw log4j log warning line with exception", sampleException);
			log42jLogger.error("This is a raw log4j log error line with exception", sampleException);
		}
		for (int i = 0; i < 10; i++) {
			log42jLogger.info("This is a slow log with number {}", i);
			Thread.sleep(1000);
		}

	}

}
