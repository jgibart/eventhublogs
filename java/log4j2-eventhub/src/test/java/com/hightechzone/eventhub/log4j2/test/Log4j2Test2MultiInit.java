package com.hightechzone.eventhub.log4j2.test;

import java.io.File;
import java.net.ConnectException;
import java.util.Date;

import org.apache.logging.log4j.CloseableThreadContext;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.hightechzone.eventhub.log4j2.Log4j2Logs;

public class Log4j2Test2MultiInit {
	public static Logger slf4jLogger = LoggerFactory.getLogger(Log4j2Test2MultiInit.class);
 
	@Parameter(names = { "-d", "--debug" }, description = "Debug mode")
	boolean debug = false;

	public static void main(String... argv) throws Exception {
		
		for ( int iter = 0; iter < 4; iter++) {
			runOnce(iter, argv);
		}
	}
	public static void runOnce(int iter,  String... argv) throws Exception {
		System.out.println("iteration "+iter);
		Log4j2Logs.simpleLogging();
		Log4j2Test main = new Log4j2Test();
		JCommander.newBuilder().addObject(main).build().parse(argv);
		try {
			if (main.debug)
				Log4j2Logs.setDebug("com.hightechzone");
			
			slf4jLogger.info("trace in iteration {}", iter);

 		} catch (Throwable t) {
			slf4jLogger.error("uncaught exception at top level ", t);
			throw t;
		} finally {
			// this call is mandatory to close everything right
			Log4j2Logs.shutdown();
		}
	} 
}
