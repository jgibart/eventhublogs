package com.hightechzone.eventhub.log4j.test;

import java.io.File;
import java.net.ConnectException;
import java.util.Date;

 import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.hightechzone.eventhub.log4j.Log4jLogs;

public class Log4jTestMultiInit {
	public static Logger slf4jLogger = LoggerFactory.getLogger(Log4jTestMultiInit.class);
 
	@Parameter(names = { "-d", "--debug" }, description = "Debug mode")
	boolean debug = false;

	public static void main(String... argv) throws Exception {
		
		for ( int iter = 0; iter < 4; iter++) {
			runOnce(iter, argv);
		}
	}
	public static void runOnce(int iter,  String... argv) throws Exception {
		System.out.println("iteration "+iter);
		Log4jLogs.simpleLogging();
		Log4jTestMultiInit main = new Log4jTestMultiInit();
		JCommander.newBuilder().addObject(main).build().parse(argv);
		try {
			if (main.debug)
				Log4jLogs.setDebug("com.hightechzone");
			
			slf4jLogger.info("this is my log test in iteration {}", iter);

 		} catch (Throwable t) {
			slf4jLogger.error("uncaught exception at top level ", t);
			throw t;
		} finally {
			// this call is mandatory to close everything right
			Log4jLogs.shutdown();
		}
	} 
}
