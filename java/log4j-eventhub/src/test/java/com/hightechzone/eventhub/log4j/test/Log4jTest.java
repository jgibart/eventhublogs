package com.hightechzone.eventhub.log4j.test;

import java.io.File;
import java.net.ConnectException;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.hightechzone.eventhub.log4j.Log4jLogs;

public class Log4jTest {
	public static Logger slf4jLogger = LoggerFactory.getLogger(Log4jTest.class);
	public static org.apache.log4j.Logger log4jLogger = org.apache.log4j.Logger
			.getLogger(Log4jTest.class);

	@Parameter(names = { "-d", "--debug" }, description = "Debug mode")
	boolean debug = false;

	public static void main(String... argv) throws Exception {
		runOnce(argv) ;
		runOnce(argv) ;
	}	
	public static void runOnce(String... argv) throws Exception {
		//Log4jLogs.initJustConsole() ;
		Log4jLogs.simpleLogging("{   \"debug\": true,   \"name\" : \"rtfld-rc-ehubns-we-hp\",   \"connectionString\": \"Endpoint=sb://rtfld-rc-ehubns-we-hp.servicebus.windows.net/;SharedAccessKeyName=databricks_rtfld;SharedAccessKey=6CwFaxap2UkFyHTl6UdKz6AFfaNNquQPvmANFf5AzAA=;EntityPath=databricks_notebook_logs\",   \"globalFields\": {   \"branch\" : \"TGS/TGITS\",    \"index\" :\"Dataplatform\",    \"application\" : \"RTFLD\",    \"environment\" : \"hp\",    \"source_component\" : \"RTFLD-StreamIoT\",    \"source_name\" :\"RTFLD-StreamIoT\"    } }");
		Log4jTest main = new Log4jTest();
		JCommander.newBuilder().addObject(main).build().parse(argv);
		try {
			if (main.debug)
				Log4jLogs.setDebug("com.hightechzone");
			main.testDirectLog4j();
			main.testSlf4j();
		} catch (Throwable t) {
			slf4jLogger.error("uncaught exception at top level ", t);
			throw t;
		} finally {
			// this call is mandatory to close everything right
			Log4jLogs.shutdown();
		}
	}
	public void testSlf4j() throws Exception {
		MDC.put("username", "jocelyn");
		slf4jLogger.trace("This is a SLF4j -> log4j log trace line");
		slf4jLogger.debug("This is a SLF4j -> log4j log debug line");
		slf4jLogger.info("This is a SLF4j -> log4j log info line");
		slf4jLogger.info("This is a parametrized SLF4j -> log4j  parameter1 {}, parameter2 {}, parameter3 {}",
				new Object[] { "someValue", new File("."), new Date() });
		slf4jLogger.warn("This is a SLF4j -> log4j log warning line");
		slf4jLogger.error("This is a SLF4j -> log4j log error line");
		MDC.remove("username");

		try {
			MDC.put("username", "raymond");
		
			Exception sampleException = new ConnectException("dont panic this is just a fake connect exception");
			slf4jLogger.warn("This is a SLF4j -> log4j log warning line with exception", sampleException);
			slf4jLogger.error("This is a SLF4j -> log4j log error line with exception", sampleException);

		}finally {
			MDC.remove("username");
			
		}
		for (int i = 0; i < 10; i++) {
			slf4jLogger.info("This is a slow log with number {}", i);
			Thread.sleep(1000);
		}
	}
	public void testDirectLog4j() throws Exception {

		org.apache.log4j.MDC.put("username", "jocelyn");
		log4jLogger.trace("This is a  log4j log trace line");
		log4jLogger.debug("This is a  log4j log debug line");
		log4jLogger.info("This is a  log4j log info line");
		log4jLogger.info(String.format("This is not a parametrized   log4j  parameter1 %s, parameter2 %s, parameter3 %s",
				 "someValue", new File("."), new Date() ));
		log4jLogger.warn("This is a SLF4j -> log4j log warning line");
		log4jLogger.error("This is a SLF4j -> log4j log error line");
		org.apache.log4j.MDC.remove("username");
		try {
			org.apache.log4j.MDC.put("username", "raymond");
		
			Exception sampleException = new ConnectException("dont panic this is just a fake connect exception");

			log4jLogger.trace("This is a raw log4j log trace line");
			log4jLogger.debug("This is a raw log4j log debug line");
			log4jLogger.info(String.format("This is a log4j parametrized info line parameter1 %s, parameter2 %s, parameter3 %s",
					 "someValue", new File("."), new Date()) );
			log4jLogger.warn("This is a raw log4j log warning line");
			log4jLogger.error("This is a raw log4j log error line");

			log4jLogger.warn("This is a raw log4j log warning line with exception", sampleException);
			log4jLogger.error("This is a raw log4j log error line with exception", sampleException);
		} finally {
			org.apache.log4j.MDC.remove("username");
		}
		for (int i = 0; i < 5; i++) {
			log4jLogger.info("This is a slow log with number "+ i);
			Thread.sleep(1000);
		}

	}

}
