package com.hightechzone.eventhub.log4j;

import java.io.Serializable;

import org.apache.log4j.Layout;
import org.apache.log4j.spi.Filter;

 
public class ConfigObj {
	  String name = null;
      Layout layout = null;
      boolean ignoreExceptions= false;
      String connectionString = null;
      boolean immediateFlush = false;
      boolean debug = false;
}
