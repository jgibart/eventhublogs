// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package com.hightechzone.eventhub.log4j;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import com.microsoft.azure.eventhubs.ConnectionStringBuilder;
import com.microsoft.azure.eventhubs.EventData;
import com.microsoft.azure.eventhubs.EventHubClient;
import com.microsoft.azure.eventhubs.EventHubException;
import com.microsoft.azure.eventhubs.RetryPolicy;
import com.microsoft.azure.eventhubs.impl.ClientConstants;
import com.microsoft.azure.eventhubs.impl.RetryExponential;

public final class EventHubsManager {
	private ScheduledExecutorService EXECUTOR_SERVICE = null;
	private final String eventHubConnectionString;
	private EventHubClient eventHubSender;
	private boolean debug;

	protected EventHubsManager(final String name, final String eventHubConnectionString, final boolean debug) {
		this.debug = debug;
		this.eventHubConnectionString = eventHubConnectionString;
	}

	public void send(final byte[] msg) throws EventHubException {
		if (msg != null && eventHubSender != null) {
			EventData data = EventData.create(msg);
			this.eventHubSender.sendSync(data);
		}
	}

	public synchronized void send(AtomicInteger currentBufferedSizeBytes, final ConcurrentLinkedQueue<byte[]> messages)
			throws EventHubException {
		if (messages != null && eventHubSender != null) {
			LinkedList<EventData> events = new LinkedList<EventData>();
			byte[] data = messages.poll();
			while (data != null) {
				if (debug)
					System.out.println(new String(data, StandardCharsets.UTF_8));
				events.add(EventData.create(data));
				if (currentBufferedSizeBytes != null)
					currentBufferedSizeBytes.addAndGet(-1 * data.length);
				data = messages.poll();
			}
			this.eventHubSender.sendSync(events);
			if (debug)
				System.out.println("sent " + events.size() + " events to eventhub");
		}
	}

	public void startup() throws Exception {
		ConnectionStringBuilder csb = new ConnectionStringBuilder(this.eventHubConnectionString)
				.setOperationTimeout(Duration.ofMillis(10000));
		RetryPolicy retry = new RetryExponential(Duration.ofSeconds(0), Duration.ofSeconds(5), 3, "azurelogging");
		// java.lang.NoSuchMethodError:
		// com.microsoft.azure.eventhubs.EventHubClient.createSync(Ljava/lang/String;Lcom/microsoft/azure/eventhubs/RetryPolicy;Ljava/util/concurrent/ScheduledExecutorService;)Lcom/microsoft/azure/eventhubs/EventHubClient;
		// jeez ! Api is unstable and keeps on changing !!!
		// com.microsoft.azure:azure-eventhubs-spark_2.11:2.3.13 had
		// EventHubClient.createSync
		// com.microsoft.azure:azure-eventhubs-spark_2.11:2.3.14.1 has
		// this.eventHubSender = EventHubClient.createSync(csb.toString(),retry,
		// EXECUTOR_SERVICE);

		Method createMethod = null;
		try {
			createMethod = EventHubClient.class.getMethod("createSync", String.class, RetryPolicy.class,
					ScheduledExecutorService.class);

		} catch (Exception e) {
			createMethod = EventHubClient.class.getMethod("createFromConnectionStringSync", String.class,
					RetryPolicy.class, ScheduledExecutorService.class);

		}
		EXECUTOR_SERVICE = Executors.newScheduledThreadPool(1);
		try {
			this.eventHubSender = (EventHubClient) createMethod.invoke(null, csb.toString(), retry, EXECUTOR_SERVICE);
		} catch (Exception e) {
			try {
				EXECUTOR_SERVICE.shutdownNow();
			} catch (Exception e2) {
				System.out.println("caught exception on shutdownNow " + e2.toString());
			}
			EXECUTOR_SERVICE = null;
			throw e;
		}
	}

	public void shutdown() {
		if (eventHubSender != null) {
			try {
				//dont close sync ...
				eventHubSender.closeSync();
			} catch (Exception e) {
				System.out.println("caught exception on close " + e.toString());

			}
			eventHubSender = null;
		}
		if (EXECUTOR_SERVICE != null) {
			try {
				Thread.sleep(100);
				EXECUTOR_SERVICE.shutdownNow();
			} catch (Exception e) {
				System.out.println("caught exception on shutdownNow " + e.toString());

			}
			EXECUTOR_SERVICE = null;
		}
	}

}
