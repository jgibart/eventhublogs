// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package com.hightechzone.eventhub.log4j;


import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.pattern.LogEvent;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;

 
/**
 * Sends {@link LogEvent}'s to Microsoft Azure EventHubs. By default, tuned for
 * high performance and hence, pushes a batch of Events.
 */
public final class EventHubsAppender extends  AppenderSkeleton {
	private static final int MAX_BATCH_SIZE_BYTES = 8 * 1024;

	// this constant is tuned to use the MaximumAllowedMessageSize(256K) including
	// AMQP-Headers for a LogEvent of 1Char
	private static final int secondsTillSend = 5;
	private static final int MAX_BATCH_SIZE = 21312;
	private static final long serialVersionUID = 1L;

	private final EventHubsManager eventHubsManager;
	private final boolean immediateFlush;
	private final boolean debug;
	private   boolean locallyStoped = false;;
	private final AtomicInteger currentBufferedSizeBytes;
	private final ConcurrentLinkedQueue<byte[]> logEvents;

	private static boolean noticeDisplayed = false;
	FlusherThread flusherThread = null;

	public void disableNotice() {
		noticeDisplayed = true;
	}

	private EventHubsAppender(final String name, final Layout layout,
			final boolean ignoreExceptions, final EventHubsManager eventHubsManager, final boolean immediateFlush, final boolean debug) {

		setName(name);
		this.eventHubsManager = eventHubsManager;
		this.immediateFlush = immediateFlush;
		this.debug= debug;
		this.logEvents = new ConcurrentLinkedQueue<byte[]>();
		this.currentBufferedSizeBytes = new AtomicInteger();
		this.layout =  layout ;
		

	}

	long nextFlush = System.currentTimeMillis()+1000*secondsTillSend;
	protected long getDelayTillFlush(long current) {
 		return  Math.max(nextFlush - current  , 0);
	}
 
	private void maybeFlush(long current) {
		if (  current < nextFlush) 
			return;
		flush();
	}



	public static EventHubsAppender createAppender(
			final String name,
			Layout layout,
			 final boolean ignoreExceptions,
			 final String connectionString,
			 final boolean immediateFlush,
			 final boolean debug) {
		final EventHubsManager eventHubsManager = new EventHubsManager(name, connectionString, debug);
		return new EventHubsAppender(name, layout, ignoreExceptions, eventHubsManager, immediateFlush, debug);
	}


	@Override
	protected void append(LoggingEvent logEvent) {
		if ( locallyStoped )
			return;
		String serializedLogEventString = null;
		byte[] serializedLogEvent = null;
		if (!noticeDisplayed) {

			noticeDisplayed = true;
			System.err.println("Logging to event hub " + getName() + " in place.");
		}
		try {
			Layout  layout = getLayout();

			if (layout != null) {
				serializedLogEventString  = layout.format(logEvent);
			} else {
				serializedLogEventString  = logEvent.getMessage().toString();
			}
			serializedLogEvent = serializedLogEventString.getBytes(StandardCharsets.UTF_8);
			if (serializedLogEvent != null) {
				if (this.immediateFlush) {
					this.eventHubsManager.send(serializedLogEvent);
					return;
				} else {
					nextFlush  = System.currentTimeMillis() +1000*secondsTillSend;
					int currentSize = this.currentBufferedSizeBytes.addAndGet(serializedLogEvent.length);
					this.logEvents.offer(serializedLogEvent);
					int count = this.logEvents.size();
					if (currentSize < EventHubsAppender.MAX_BATCH_SIZE_BYTES
							&& this.logEvents.size() < EventHubsAppender.MAX_BATCH_SIZE && logEvent.getTimeStamp() < nextFlush ) {
						return;
					}
 
 					this.eventHubsManager.send(this.currentBufferedSizeBytes, this.logEvents);
				}
			}
		} catch (final Throwable exception) {

			System.err
					.println(String.format(Locale.US, "[%s] Appender failed to logEvent to EventHub.", this.getName()));
			exception.printStackTrace(System.err);
			// remove the current LogEvent from the inMem Q - to avoid replay
            
		}
	}

	class FlusherThread extends Thread {
		public FlusherThread() {
			super("logs flushing thread");
		}

		public void run() {
			synchronized (this) {
				try {
					long current = System.currentTimeMillis();
					while (!isFinished()) {
						long waitTime = getDelayTillFlush(current);
						if (waitTime > 0) {
							wait(waitTime);
						}
						current = System.currentTimeMillis();
						maybeFlush(current);

					}
				} catch (InterruptedException e) {
				}
			}

		}

		void setFinished() {
			finished = true;
			synchronized (this) {
				notify();
			}

		};

		boolean isFinished() {
			return finished;
		};

		boolean finished = false;
	}

	public void start()   {

		try {
			this.eventHubsManager.startup();
			if (!immediateFlush) {
				flusherThread = new FlusherThread();

				flusherThread.setDaemon(true);
				flusherThread.start();
			}

		} catch (Throwable exception) {
			final String errMsg = String.format(Locale.US, "[%s] Appender initialization failed with error: [%s]",
					this.getName(), exception.getMessage());

			System.err.println(errMsg);
			exception.printStackTrace(System.err);
			throw new RuntimeException(exception);
		}
	}

	public synchronized void flush() {
		if ( locallyStoped ) {
			return;
		}
		if (debug) {
			System.out.println("flush called");
		}
		nextFlush  = System.currentTimeMillis() +1000*secondsTillSend;
		if (this.immediateFlush) {
			return;
		}
		if (this.logEvents.size() == 0) {
			return;
		}

		try {

 			this.eventHubsManager.send(this.currentBufferedSizeBytes, this.logEvents);

		} catch (final Throwable exception) {
			System.err
					.println(String.format(Locale.US, "[%s] Appender failed to logEvent to EventHub.", this.getName()));
			exception.printStackTrace(System.err);
			// remove the current LogEvent from the inMem Q - to avoid replay

		}

	}


	@Override
	public void close() {
		
		flush();
		if (flusherThread != null) {
			flusherThread.setFinished();
			flusherThread = null;
		}
		locallyStoped  = true;
		this.eventHubsManager.shutdown();
	}

	@Override
	public boolean requiresLayout() {
		return false;
	}

}
