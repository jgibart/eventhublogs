package com.hightechzone.eventhub.log4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.PatternLayout;
import org.json.JSONObject;

public class Log4jLogs {
	public static Logger logger = LogManager.getLogger(Log4jLogs.class);

	private static boolean silenceMicrosoft = true;

	public void dontSilenceMicrosoftlogs() {
		silenceMicrosoft = false;

	}

	static EventHubsAppender evtAppender = null;
	static public boolean eventhubEnabled = true;

	public static void initJustConsole() throws Exception {
//		LogManager.resetConfiguration();
//		 Logger.getRootLogger().getLoggerRepository().resetConfiguration();
		Logger root = Logger.getRootLogger();
		ConsoleAppender console = new ConsoleAppender();
		String PATTERN = "%d{ISO8601} [%p|%c|%C{1}] %m%n";
		console.setLayout(new PatternLayout(PATTERN));
		console.setTarget("System.out");
		console.setThreshold(Level.TRACE);
		console.activateOptions();
		root.addAppender(console);

	}

	public static void simpleLogging() throws Exception {
		simpleLogging(null);
	}

	public static void simpleLogging(String jsonConfig) throws Exception {
		// log4j logging
		setToLevel("", Level.WARN);
		setToLevel("com.hightechzone", Level.INFO);
		if (silenceMicrosoft)
			setToLevel("com.microsoft.azure", Level.WARN);

		if (eventhubEnabled) {

			Logger root = Logger.getRootLogger();
			ConfigObj eventHubConf = loadEventHubProperties(jsonConfig);
			if (eventHubConf.layout == null) {
				eventHubConf.layout = new JsonLayout();
			}
			if (evtAppender == null) {
				evtAppender = EventHubsAppender.createAppender(eventHubConf.name, eventHubConf.layout,
						eventHubConf.ignoreExceptions, eventHubConf.connectionString, eventHubConf.immediateFlush,
						eventHubConf.debug);
				
				evtAppender.activateOptions();
				evtAppender.start();
			}
			root.addAppender(evtAppender);
		}

	}

	private static ConfigObj loadEventHubProperties(String jsonConfig) throws IOException {
		ConfigObj ret = new ConfigObj();
		if (jsonConfig == null) {
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("eventhubconfig.properties");
			if (is == null)
				throw new IllegalStateException("eventhubconfig.properties not in classpath ");
			try {
				Properties props = new Properties();
				props.load(is);
				if (props.containsKey("eventhub.name"))
					ret.name = BasicTemplating.getInterPolatedString(props.getProperty("eventhub.name"));
				if (props.containsKey("eventhub.debug"))
					ret.debug = Boolean.parseBoolean(props.getProperty("eventhub.debug"));
				if (props.containsKey("eventhub.connectionString"))
					ret.ignoreExceptions = Boolean.parseBoolean(props.getProperty("eventhub.ignoreExceptions"));
				if (props.containsKey("eventhub.connectionString"))
					ret.connectionString =BasicTemplating.getInterPolatedString( props.getProperty("eventhub.connectionString"));
				if (props.containsKey("eventhub.immediateFlush"))
					ret.immediateFlush = Boolean.parseBoolean(props.getProperty("eventhub.immediateFlush"));
				for (Entry<Object, Object> ent : props.entrySet()) {
					String key = (String) ent.getKey();
					if (key.startsWith("eventhub.globalFields.")) {
						MDC.put(key.substring("eventhub.globalFields.".length()), BasicTemplating.getInterPolatedString((String) ent.getValue()));
					}
				}
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			JSONObject config = new JSONObject(jsonConfig);
			if ( config.has("debug")) {
				ret.debug = config.getBoolean("debug");
			}
			if ( config.has("name")) {
				ret.name= config.getString("name");
			}
			if ( config.has("ignoreExceptions")) {
				ret.ignoreExceptions= config.getBoolean("ignoreExceptions");
			}
			if ( config.has("connectionString")) {
				ret.connectionString= config.getString("connectionString");
			}
			if ( config.has("immediateFlush")) {
				ret.immediateFlush= config.getBoolean("immediateFlush");
			}
				
			if ( config.has("globalFields")) {
				JSONObject globalFields = config.getJSONObject("globalFields");
				for ( String key : globalFields .keySet()) {
					MDC.put(key, globalFields .getString(key));
				}
			}

		}
		return ret;
	}

	static Logger getLogger(String name) {
		Logger targetLogger = LogManager.getRootLogger();
		if (name != null && !name.isEmpty())
			targetLogger = LogManager.getLogger(name);
		return targetLogger;
	}

	public static void setToLevel(String name, Level lev) {
		Logger logger = Logger.getLogger(name);
		logger.setLevel(lev);
	}

	public static void ensureLevel(String name, Level lev) {
		Logger targetLogger = getLogger(name);
		int targetLogLev = lev.toInt();
		if (targetLogger.getLevel() != null && targetLogger.getLevel().toInt() < targetLogLev) {
			logger.warn(
					"downgrading to level " + lev + " instead of " + targetLogger.getLevel() + " for logger " + name);
		}
		targetLogger.setLevel(lev);

	}

	public static void ensureWarning(String name) {
		ensureLevel(name, Level.WARN);
	}

	public static void setDebug(String name) {
		setToLevel(name, Level.DEBUG);
	}

	public static void setDebug() {
		setDebug("");
	}

	public static void flush() throws Exception {
		if (evtAppender != null)
			evtAppender.flush();

	}

	public static void shutdown() throws Exception {
		if (evtAppender != null) {
			Logger root = Logger.getRootLogger();

			root.removeAppender(evtAppender);
			evtAppender.close();
			evtAppender = null;
		}



	}

	/**
	 * method responsible to add the email appender to a specific logger.
	 *
	 * @param name
	 * @param alerter
	 */

}
