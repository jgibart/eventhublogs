package com.hightechzone.eventhub.log4j;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;

import org.apache.log4j.Layout;
import org.apache.log4j.MDC;
import org.apache.log4j.spi.LoggingEvent;
import org.json.JSONObject;

public class JsonLayout extends Layout {
	static final String CONTENT_TYPE = "application/json";
	static final Charset charset = StandardCharsets.UTF_8;
	AtomicLong counter = new AtomicLong();

	static ThreadLocal<SimpleDateFormat> fmt = new ThreadLocal<SimpleDateFormat>() {
		protected SimpleDateFormat initialValue() {
			// SimpleDateFormat is not htread safe. Here is the cure
			SimpleDateFormat ret = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			ret.setTimeZone(TimeZone.getDefault());
			return ret;
		}

	};

	// "logger_name": record.name, "log_message" : record.message}

	private String toJsonString(LoggingEvent record) {
		final JSONObject jo = new JSONObject();
		jo.put("timestamp", fmt.get().format(new Date(record.getTimeStamp())));
		jo.put("criticality",record.getLevel().toString());
		//jo.put("sequence", counter.incrementAndGet());
		jo.put("logger_name",record.getLoggerName().replaceAll(".*\\.", ""));
		jo.put("log_message",record.getMessage().toString());


		Map<String,String> contextData = MDC.getContext();
		if (contextData != null && contextData.size() > 0) {
			contextData.forEach((key, value) -> {
				jo.put((String)key , value.toString());

			});

		}
		if (record.getThrowableInformation() != null) {
			jo.put("exception", record.getThrowableInformation().getThrowable().toString());
		}
		return jo.toString()+"\n";

	}


	@Override
	public String format(LoggingEvent event) {
		return toJsonString(event);
	}

	@Override
	public boolean ignoresThrowable() {
		return false;
	}

	@Override
	public void activateOptions() {

	}

}
