# AzureLogging solution framework for standalone python ( not spark)


##  description

We are requested to publish applications logs of various programs ( scripts, batch jobs, servers, ... ) to Azure eventhubs which is our gateway towards the ELK monitoring infrastructure.
In order to ease the process we are delivering a set of (small) apis to allow logging to eventhubs in json format, along with environmental context values, using the different languages natural logging apis.



On python  we permit the use of the logging logers standard package


 
## requirements

Only python 3 is supported

Your program must use the egg azurelogging-0.4-py3.6.egg from dist subdirectory
Your program must also use the egg azure-eventhub==5.0.0b5 or azure eventhubs access


Warning : azure-eventhub==5.0.0b5 , not any version below, because due to beaking changes the versions below are not compatible.



## usage
The SampleApp.py program shows the loggers usage :

import  the required modules
protect your main code with a try finally block


```
#required imports
import logging
import azurelogging
import eventhubconfig


# your code
def run():
  ....


#initalize logging
config=eventhubconfig.getConfig()
azurelogging.setupLogs(config)
logger = azurelogging.getLogger(__name__) 
logger.setLevel(logging.INFO)


try:
        run()
finally:
        #ensure shutdown is called whatever
        #logger.info("shutdown")
        azurelogging.shutdown()

```


## extra capabilities of loggers
loggers have a non standard capability using this module : pass named parameters

In general it is a good practice to pass all varying information in message logs as parameters :
```
 logger.info("message whith unnamed parameter %s ", "stringparam")
```

which will generate in json   
```
  { "message_fmt" : "message whith unnamed parameter %s ", "arg0": "stringparam" }
  
```
instead of just 
```
  { "message" : "message whith unnamed parameter stringparam" }

```


Even better , now you can name the arguments :
```
logger.info("message whith first_name argument = {first_name}  ", first_name="remi")
```

which will generate in json something like 
```
  { "message_fmt" : "message whith first_name argument = {first_name}  ", "first_name": "remi" }
  
```

## config


the configuration here has been exported in a json code returning a config object ;
eventhubconfig.py

```


def getConfig():

    return {
        "debug":False,
        "name" : "<eventhubnamespace>",
        "ignoreExceptions":  False,
        "connectionString": "Endpoint=sb://<eventhubnamespace>.servicebus.windows.net/;SharedAccessKeyName=xxxxxxxxxxxx;SharedAccessKey=yyyyyyyyyyyyyyyyyyyyyy;EntityPath=<eventhub>",
        "globalFields": {
          "branch" : "TGS/TGITS", 
          "index" :"Dataplatform", 
          "application" : "<application name>", 
          "environment" : "<environment hp | prod | preprod", 
          "source_component" : "<program name>", 
          "source_name" :"<machine name>"
        }
}

```