from setuptools import setup, find_packages
setup(
    name = "azurelogging",
    version = "0.4",
    packages = find_packages(),
    description='coupling python loggers to event hub for application logs monitoring',
    author='Jocelyn Gibart',
    author_email='jocelyn@hightechzone.fr',
    install_requires=[
       'azure-eventhub==5.0.0b5',
    ]
 )
