import logging
from  logging.handlers import MemoryHandler;
from  logging  import Logger,Manager;
from logging import StreamHandler
import json
import datetime
import time
import sys
import os
import re
import threading
import __main__
import azure.eventhub
from azure.eventhub import EventHubProducerClient, EventData
 
from inspect import getargspec
from logging import RootLogger

__version__ = "0.4"
threadLocalContexts = threading.local()

class BraceMessage(object):
    def __init__(self, fmt, args, kwargs):
        self.fmt = fmt
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return str(self.fmt).format(*self.args, **self.kwargs)

class StyleAdapter(logging.LoggerAdapter):
    def __init__(self, logger):
        self.logger = logger

    def log(self, level, msg, *args, **kwargs):
        if self.isEnabledFor(level):
            #msg, log_kwargs = self.process(msg, kwargs)
            #self.logger._log(level, BraceMessage(msg, args, kwargs), (), 
            #        **log_kwargs)
            _replaceLog(self.logger, level, msg,  args, **kwargs )
    def handlers(self):
        return self.logger.handlers

    def process(self, msg, kwargs):
        return msg, {key: kwargs[key] 
                for key in getargspec(self.logger._log).args[1:] if key in kwargs}

class ExtendedLogRecord(logging.LogRecord):
    """
    A root logger is not that different to any other logger, except that
    it must have a logging level and there is only one instance of it in
    the hierarchy.
    """
    def __init__(self, logger, level, fn, lno, brmsg, 
                                 exc_info, func, extra, sinfo, ctxArgs):
        """
        Initialize the logger with the name "root".
        """
        logging.LogRecord.__init__( self, logger.name,  level, fn, lno, str(brmsg), (), 
                                 exc_info, func=func, extra=extra, sinfo=sinfo)
        self.fmt = brmsg.fmt
        self.kwargs = brmsg.kwargs
        self.args = brmsg.args
        self.ctxArgs = ctxArgs
        
            



def  makeExtendedRecord(logger, level, fn, lno, msg, 
                                 exc_info, func, extra, sinfo, args,kwargs):
                
                 
     brmsg = BraceMessage(msg, args, kwargs)
     #print (str(brmsg))
     #ret = logger.makeRecord(logger.name, level, fn, lno, brmsg, args, 
     #                            exc_info, func, extra, sinfo)
     #ret["messageFormat"] = msg
     #if len ( kwargs ) > 0:
     #   ret["parameters"] = kwargs
     ctxArgs = {}
     if kh is not None:
        for k,v in kh.contexts.items():
           ctxArgs[k] = v
     if  hasattr( threadLocalContexts, "x" ):
        for k,v in threadLocalContexts.x.items():
           ctxArgs[k] = v
     
     ret = ExtendedLogRecord(logger, level, fn, lno, brmsg, 
                                 exc_info, func, extra, sinfo, ctxArgs)
     return ret

_srcfile = os.path.normcase(makeExtendedRecord.__code__.co_filename)
        
reserved = [ "branch", "index", "application", "environment", "criticality", "log_message", "source_component", "source_name" ]
        
#exc_info=None, extra=None, stack_info=False, stacklevel=1
def  _replaceLog(logger, level, msg,  args, **kwargs ):
        """
        fmtstring = msg
        Low-level logging routine which creates a LogRecord and then calls
        all the handlers of this logger to handle the record.
        """
        log_kwargs =   {key: kwargs[key] 
                for key in getargspec(logger._log).args[1:] if key in kwargs}
        stacklevel=log_kwargs["stacklevel"] if "stacklevel" in log_kwargs else 1
        exc_info= log_kwargs["exc_info"] if "exc_info" in log_kwargs else  None
        extra=log_kwargs["extra"] if "extra" in log_kwargs else None
        stack_info=log_kwargs["stack_info"] if "stack_info" in log_kwargs else False

        
        sinfo = None
        if _srcfile:
            #IronPython doesn't track Python frames, so findCaller raises an
            #exception on some versions of IronPython. We trap it here so that
            #IronPython can use logging.
            try:
                fn, lno, func, sinfo = logger.findCaller(stack_info)
            except ValueError: # pragma: no cover
                fn, lno, func = "(unknown file)", 0, "(unknown function)"
        else: # pragma: no cover
            fn, lno, func = "(unknown file)", 0, "(unknown function)"
        if exc_info:
            if isinstance(exc_info, BaseException):
                exc_info = (type(exc_info), exc_info, exc_info.__traceback__)
            elif not isinstance(exc_info, tuple):
                exc_info = sys.exc_info()
        #print ("processing fmt %s args %s kwargs %s"%(msg, str(args), str(kwargs)))
        record = makeExtendedRecord(logger, level, fn, lno, msg,
                             exc_info, func, extra, sinfo, args, kwargs)
        logger.handle(record)
            
class WrappedRootLogger(logging.RootLogger):
    """
    A root logger is not that different to any other logger, except that
    it must have a logging level and there is only one instance of it in
    the hierarchy.
    """
    def __init__(self, level):
        """
        Initialize the logger with the name "root".
        """
        logging.RootLogger.__init__(  self,  level)

    def process(self, msg, kwargs):
        return msg, {key: kwargs[key] 
                for key in getargspec(self._log).args[1:] if key in kwargs}

    def _log(self, level, msg, args, **kwargs):
        if self.isEnabledFor(level):
            _replaceLog(self, level, msg,  args, **kwargs )
        
        
class EventHubHandler(MemoryHandler):
    def close(self):
        self.cancel_future_calls()
        self.flush()
        MemoryHandler.close(self)
        if self.debug:
          print ("closing")
        if self.client is not None:
                self.client .close()
                self.client = None
                self.client = None
            
        
    def flush_repeatedly(self, interval):
        stopped = threading.Event()
        def loop():
            while not stopped.wait(interval): # the first call is in `interval` secs
                self.flush()
        threading.Thread(target=loop).start()    
        return stopped.set
    
    def __init__(self, config):
        self.client = None
        self.config = config
        self.debug = False
        if "debug" in config :
          self.debug = config ["debug"]
        if self.debug and "globalFields" in self.config:
          for k,v in self.config["globalFields"].items():
            print ("Global field from configuration %s=%s"%(k,v))
          
        self.contexts = {}
        MemoryHandler.__init__(self, 100 )
        self.config = config
        # EventHub Configuration
        print ("using azure.eventhub version "+azure.eventhub.__version__ + " (expecting >= 5.0.0b5  or better)")
        self.client = EventHubProducerClient.from_connection_string(config ["connectionString"])
        self.cancel_future_calls = self.flush_repeatedly(15)

    def flush(self):
        """
        For a MemoryHandler, flushing means just sending the buffered
        records to the target, if there is one. Override if you want
        different behaviour.
        The record buffer is also cleared by this operation.
        """
        if self.client is not None:
            self.acquire()
            try:
                if  self.debug:
                  print ("flushing")
                #event_list = []
                event_data_batch = self.client.create_batch(max_size=64000)
                while len( self.buffer)  > 0:
                        record = self.buffer.pop()
                        date = datetime.datetime.fromtimestamp(record.created+record.msecs/1000., datetime.timezone.utc).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]+'Z'
                        jsondata = { "timestamp": date , "criticality": record.levelname, "logger_name": record.name, "log_message" : record.message}
                        if record.message != record.fmt:
                          jsondata["message_format"] = record.fmt
                        if len(record.args)>0:                          
                          argsObj = {}
                          jsondata["message_args"] = argsObj
                          for i, v in enumerate(record.args):
                            argsObj["arg"+str(i)] = v
                        if len(record.kwargs)>0:
                          jsondata["message_parameters"] = record.kwargs
                        if "globalFields" in self.config:
                            for k,v in self.config["globalFields"].items():
                                jsondata[k] = v
                        if len(record.ctxArgs) >0:
                          for k,v in record.ctxArgs.items():
                            jsondata[k] = v
                           
                        if self.debug:
                          print(json.dumps(jsondata ))
                        eventData = EventData(json.dumps(jsondata ).encode("utf-8")  )
                        try:
                            event_data_batch.try_add(eventData)
                        except ValueError:
                            if self.debug:
                                print ("sending "+str( len(event_data_batch) )+" events")
                            self.client.send(event_data_batch)
                            event_data_batch = self.client.create_batch(max_size=64000)
                            if self.debug:
                                print ("sent "+str( len(event_data_batch) )+" events")
                            event_data_batch.try_add(eventData)
                            
                if len(event_data_batch) >0:
                    if self.debug:
                        print ("sending "+str( len(event_data_batch))+" events")
                    self.client.send(event_data_batch)
                    if self.debug:
                        print ("sent "+str( len(event_data_batch))+" events")
                else:
                    if self.debug:
                        print ("nothing to flush")
            finally:
                self.release()


kh = None
def setupLogs(configp):
        #logging.basicConfig(
        #    format='%(asctime)s %(levelname)s %(message)s', 
        #    level=logging.INFO, 
        #    datefmt='%Y-%m-%dT%I:%M:%S %p'
        #    )
        global kh
        rootlogger = WrappedRootLogger(logging.WARNING)
        logging.root  = rootlogger
        logging.Logger.root  = rootlogger
        logging.Logger.manager = logging.Manager(Logger.root)
        #rootlogger =logging.root  
        for h in rootlogger.handlers[:]:
                rootlogger.removeHandler(h)
                h.close()
        ch = logging.StreamHandler(stream = sys.stdout )
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        rootlogger.addHandler(ch)
    
        config = configp
        
        rootlogger.addHandler(ch)
        kh = EventHubHandler(config)
        kh.setLevel(logging.DEBUG)
        
        rootlogger.addHandler(kh)
def getLogger(name):
   return StyleAdapter(logging.getLogger(name))

def putGlobalContext(key, value):
   if kh is not None : 
      if kh.debug:
         print ("adding to global context %s=%s"%(key,value))
      kh.contexts[key] = value
def putThreadLocalContext(key, value):
    if not hasattr( threadLocalContexts, "x" ):
      threadLocalContexts.x = {}
    if kh is not None and kh.debug: 
         print ("adding to thread local  context %s=%s"%(key,value))
    threadLocalContexts.x[key] = value

def removeThreadLocalContext(key):
      val = "<unset>"
      if  hasattr( threadLocalContexts, "x" ) and key in threadLocalContexts.x:
        val = threadLocalContexts.x.pop(key) 
      if kh is not None and kh.debug: 
         print ("removing from thread local context %s=%s"%(key, val ))

def removeGlobalContext(key):
   if kh is not None : 
      kh.flush()
      val = "<unset>"
      if key in kh.contexts:
        val = kh.contexts.pop(key) 
      if kh.debug:
         print ("removing from global context %s=%s"%(key, val ))

def shutdown():
   kh.close()
