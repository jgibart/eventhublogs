import logging
import azurelogging
import eventhubconfig
 
import time

class Main:
    def __init__(self):
        config=eventhubconfig.getConfig()
        azurelogging.setupLogs(config)
        print(str(config))
    def run(self):
        self.logger = azurelogging.getLogger(__name__) 
        self.logger.setLevel(logging.INFO)
        azurelogging.putGlobalContext("moodOftheDay", "happy")
        azurelogging.putThreadLocalContext("username", "jocelyn")
        self.logger.debug("debug message ")
        self.logger.info("info message ")
        self.logger.warning("warning message ")
        self.logger.error("error message ")
        self.logger.info("debug message whith named argument {namedArg} ", namedArg="somevalue")
        self.logger.info("debug message whith unnamed argument %s ", "somevalue")
        azurelogging.removeThreadLocalContext("username")

        logging.debug("logging debug message ")
        logging.info("logging info message ")
        logging.warning("logging warning message ")
        logging.error("logging error message ")
        logging.warning("debug message whith named argument {namedArg} ", namedArg="somevalue")
        logging.warning("debug message whith unnamed argument %s ", "somevalue")
        azurelogging.removeGlobalContext("moodOftheDay")
        azurelogging.removeGlobalContext("notset")
        azurelogging.removeThreadLocalContext("notset")
        for i in range(10):
            self.logger.info("message with parameter {messageNumber}", messageNumber=i)
            time.sleep(1)
            
if __name__ == "__main__":
    main = Main()
    try:
        main.run()
    finally:
        #print("shutdown")
        azurelogging.shutdown()
